package main

import (
	"crypto/rand"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"encoding/binary"

	"github.com/gordonklaus/portaudio"
	"github.com/pion/rtp"
	"github.com/pion/webrtc/v3"
	"gopkg.in/hraban/opus.v2"
)

var ctrlc = make(chan os.Signal)

func main() {
	// Wait for the offer to be pasted
	offer := webrtc.SessionDescription{}
	Decode(MustReadStdin(), &offer)

	// We make our own mediaEngine so we can place the sender's codecs in it.  This because we must use the
	// dynamic media type from the sender in our answer. This is not required if we are the offerer
	mediaEngine := webrtc.MediaEngine{}
	err := mediaEngine.PopulateFromSDP(offer)
	if err != nil {
		panic(err)
	}

	// Setup the opus parameters
	const channels = 2
	const sampleRate = 48000
	const pcmsize = 480
	const mtu = 1400
	PayloadTypeOpus := uint8(webrtc.DefaultPayloadTypeOpus)
	opuscodec := webrtc.NewRTPOpusCodec(PayloadTypeOpus, sampleRate)

	/* Everything below is the pion-WebRTC API, thanks for using it! */

	// Setup the codecs you want to use.
	// We'll use the default ones but you can also define your own
	mediaEngine.RegisterCodec(opuscodec)

	// Create a new RTCPeerConnection
	api := webrtc.NewAPI(webrtc.WithMediaEngine(mediaEngine))
	peerConnection, err := api.NewPeerConnection(webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	})
	if err != nil {
		panic(err)
	}

	// Initialize portaudio and buffer to recording audio
	portaudio.Initialize()
	defer portaudio.Terminate()
	pcm := make([]int16, pcmsize)

	stream, err := portaudio.OpenDefaultStream(1, 0, sampleRate, pcmsize, pcm)
	if err != nil {
		panic("Error when opening default audio stream")
	}

	defer stream.Close()
	signal.Notify(ctrlc, os.Interrupt, syscall.SIGTERM)
	go cleanup(stream)

	// Set the handler for ICE connection state
	// This will notify you when the peer has connected/disconnected
	peerConnection.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
		fmt.Printf("Connection State has changed %s \n", connectionState.String())
	})

	// Generate ssrc for future use
	ran := make([]byte, 4)
	_, err = rand.Read(ran)
	if err != nil {
		panic(err)
	}

	ssrc := binary.LittleEndian.Uint32(ran)

	// Create an audio track and add it to the peer connection
	opusTrack, err := peerConnection.NewTrack(webrtc.DefaultPayloadTypeOpus, ssrc, "audio", "pion1")
	if err != nil {
		panic(err)
	}
	_, err = peerConnection.AddTrack(opusTrack)
	if err != nil {
		panic(err)
	}

	// Set the remote SessionDescription
	if err := peerConnection.SetRemoteDescription(offer); err != nil {
		panic(err)
	}

	// Create answer
	answer, err := peerConnection.CreateAnswer(nil)
	if err != nil {
		panic(err)
	}

	// Create channel that is blocked until ICE Gathering is complete
	gatherComplete := webrtc.GatheringCompletePromise(peerConnection)

	// Sets the LocalDescription
	if err = peerConnection.SetLocalDescription(answer); err != nil {
		panic(err)
	}

	// Block until ICE Gathering is complete, disabling trickle ICE
	// we do this because we only can exchange one signaling message
	// in a production application you should exchange ICE Candidates via OnICECandidate
	<-gatherComplete

	// Output the answer in base64 so we can paste it in browser
	fmt.Println("==============HERE IS YOUR LOCAL DESCRIPTION==============")
	fmt.Println(Encode(*peerConnection.LocalDescription()))
	fmt.Println("==============HERE IS YOUR LOCAL DESCRIPTION==============")

	// Create an opus format encoder
	enc, err := opus.NewEncoder(sampleRate, 1, opus.AppVoIP)
	if err != nil {
		panic(err)
	}

	// Channel to relay recorded audio to transmit buffer
	pcmChan := make(chan []int16)

	// Start the audio recording stream
	if err := stream.Start(); err != nil {
		panic("Error when starting stream.")
	}

	/* Recording audio */
	go func() {
		for {
			err := stream.Read()
			if err != nil {
				panic("Error when reading stream.")
			}
			pcmChan <- pcm
		}
	}()

	/* Send audio */
	go func() {
		const opusbufsize = 32000

		p := make([]int16, pcmsize)
		opusdata := make([]byte, opusbufsize)

		packetizer := rtp.NewPacketizer(
			mtu,
			opuscodec.PayloadType,
			ssrc,
			opuscodec.Payloader,
			rtp.NewRandomSequencer(),
			sampleRate,
		)

		for {
			p = <-pcmChan

			n, err := enc.Encode(p, opusdata) // pcm to opus
			if err != nil {
				panic(err)
			}
			opusdata = opusdata[:n] // Remove unused space after encoding to opus

			packets := packetizer.Packetize(opusdata, pcmsize)

			for _, pack := range packets {
				opusTrack.WriteRTP(pack)
			}
		}
	}()

	select {}
}

func cleanup(stream *portaudio.Stream) {
	// User hit Ctrl-C, clean up
	<-ctrlc
	fmt.Println("Close devices")
	stream.Close()
	os.Exit(1)
}
