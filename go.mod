module bitbucket.org/slcot/portaudiosend

go 1.14

require (
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/pion/rtp v1.5.5
	github.com/pion/webrtc/v3 v3.0.0-20200703080156-ef1d5a4a8bfe
	gopkg.in/hraban/opus.v2 v2.0.0-20191117073431-57179dff69a6
)
