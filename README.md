# portaudiosend

This repository demonstrates how to send local Linux audio over a WebRTC PeerConnection using a Golang application.

### Approach Explanation
#### The Problem
Shared browsing is enabled by a VNC client and server relationship where the "sharing" feature of upgraded VNC protocol specs support control device peripherals on the server from many clients. Unfortunately, with the exeception of QEMU, most VNC servers and clients such as x11vnc and noVNC [do not support recent interest](https://github.com/novnc/noVNC/issues/302) in encoding and sharing audio between servers and clients. 

Many parties have proposed solutions to address this lack of capability. There are three "moving parts" which must be addressed.

1. How to access local audio via OS drivers
2. How to encode and send that audio to clients
3. How to decode audio on the client side

#### Clarifications
To make things easier to talk about, let us define the actors involved:

* VNC Client: A "viewer" of the remote VNC connection which can be a desktop application or a browser application like [noVNC](https://novnc.com/info.html)
* VNC Server / Target: The "host" of the VNC connection where a GUI and VNC server is running and responding to client requests (can have more than one)
* STUN/TURN Server: webrtc specific relays that facilitate connections and NAT/Traversal initiation
* Local audio input / output: The raw audio which the operating system recoginizes as being "played" which can be accessed through an audio driver such as ALSA
* [Portaudio](http://www.portaudio.com/): an abstraction on local audio drivers which fetches input streams across a variety of drivers and operating systems
* [Opus](https://opus-codec.org/): an audio encoding with gaining popularity for its sound quality and ability to encode audio with a small footprint; also commonly used on the web and in webRTC

#### Objective
Our objective is to transmit audio from VNC Server hosts to browser-based VNC clients. We hope to do so in a programatic way for maintainability, configurability, and speed. We will assume a webRTC connection already exists among the VNC client, and propose to establish a webRTC connection solely for audio between the VNC server and the VNC clients. Doing so allows for direct-to-peer audio transfer, but of course if a relay such as Jitsi bridge is used, the VNC server can connect as a
peer to the relay and repeat the same process. 

#### Benefits / Limitations
Most parties online propose the use of a "side-channel" to the VNC connection, where audio data externally is ported from server to clients. This is similar to our approach. Online, [some propose using a TCP-to-websocket adapter](https://github.com/novnc/noVNC/issues/302#issuecomment-348083184) (ex: websockify project), which can allow a user-space server application to encode audio locally and send it to browser-based clients to decode the audio. This differs from our approach in that audio decoding (step 3) must be solved by the practitioner,
whereas in our approach one can exploit existing webRTC integrations which will decode audio "for free". An alternative to our approach which is also reasonable is to use a package like [browserless](https://github.com/browserless/chrome), which is a headless Chrome browser that supports the Javascript runtime and presumably webRTC libraries for getUserMedia calls. With this approach, one can avoid implementing audio encoding (step 1) as well, but at the cost of more resource usage on the VNC server as a whole
browser runtime will be added to the overhead of other OS programs. Our approach proposes a Golang encoding and sending user-space application for low overhead on the server side. That said, our results are preliminary where we have observed an incredibly low-latency audio connection over webRTC with a mild audio quality issue we attribute to buggy code which likely only requires a few hours to fix since an "echo" style portaudio test we have performed yields clean, high quality audio. Presumably, this approach can also be used to send information over a webRTC data channel if desired for things like shared clipboard. 

#### Challenges
In relation to shared browsing, the audio transfer problem is challenging because a) few VNC servers or clients make it easy or standardized to transmit audio to/from a VNC server and client and b) accessing audio output at the operating system level from an AWS EC2 or ECS instance is an unusual and poorly supported workflow. Despite this, we can arrive at a solution with a few tricks.

#### The Approach
With terms defined, let us now talk about the approach. The proposed solution aims to:

1. Mirror VNC server audio outputs to "monitors" (i.e. dummy microphone inputs). 
2. Access the microphone stream via the portaudio library
3. Encode the microphone stream as Opus packets
4. Send the Opus packets over a WebRTC Peer Connection by writing RTP (Real-Time Transfer protocol) packets to an AudioTrack.
5. Receive and decode AudioTrack on the client side "for free".

One of the tricky details forwarding audio coming from a VNC server which is an AWS EC2 or ECS instance is that the audio driver is not straightforward to access. This is presumably because accessing audio from an AWS instance's virtualized GUI is not a common workflow. To address this, we utilize a simple trick using `pavucontrol`. This pulse audio control software allows one to simply map OS audio output drivers to "monitors" or "loopback" audio input drivers that can be used just like
microphones. This means that, for example, if the user of the VNC session visits a page on Youtube, the audio from the video the user is playing will first be piped to the OS dummy audio output on the AWS virtualized GUI and then that dummy audio output will be passed through to a "monitor" audio input which can be used for sending to remote clients. One can verify that the streams are functioning properly by using the `pavucontrol` and observing the volume meters on each input and output
device while some audio is playing on the AWS instance. Note that this output to input mapping might be possible using the portaudio library such that a seperate program is not needed to stitch the output audio to input audio, but this requires future investigation.

Once the audio is accessible through an audio input stream, the next step is to access it programmatically from a Golang application for encoding and then sending through a webRTC Peer Connection. The PortAudio library can be used for this purpose as it serves as a high-level API for accessing a variety of different audio drivers on Linux and Windows. PCM data is fed to a buffer in Golang in one go routine, and then another go rountine handles encoding the data in that buffer into Opus audio
packets. Once the data is packetized, it is ready to be written to the webRTC AudioTrack which accepts RTP packets. In this example, we do not implement the peer discovery process, and instead simply manually copy/paste the session description of the VNC client and VNC host machines. More information can be found in the Usage section below. 

### Next Steps
Currently, the audio stream quality is poor because of a crackling sound that accompanies the audio. This said it is easy to make out what audio is being played and what voices are talking in a video, but futher debugging is needed to fix these issues (more than the 8 hours I put into this so far). This said, latency is surprisingly low (video and audio streams seemed to line up, at least for the short 10 minutes I was watching Youtube). 

Also, the mirroring of audio output devices to audio inputs is something which should really be handled by the Golang code, and I have not yet looked into whether or not PortAudio can support this out-of-the-box (but it definitely should be considered).

Lastly, the webRTC peer discovery mechanism needs to be incorporated here to have the full setup.

### Dependencies
To build Go binaries and to run on the target machine, run `./install_deps.sh` in either case. This installs necessary C/C++ libraries for Opus, ALSA, and PortAudio.

Building this package also involves having Golang tooling for version 1.14.4 or similar.

Assuming pulseaudio is already installed on the target Linux machine, the last package necessary (for now) is `pavucontrol` to mirror speaker outputs to microphone inputs. Running `sudo apt install pavucontrol` will install this program.

### Building
After the dependencies have been installed, cd to the `portaudiosend` repo and run `go build ./`. The output binary is `portaudiosend` and expects a webRTC base64 encoded session description for the remote webRTC peer. This program **cannot** run without this input string, so read below how to use this binary.

### Setting up an instance for the demo
I personally used this [guide](https://aws-labs.com/set-desktop-gui-amazon-ec2-instance/) to setup an AWS instance with Ubuntu 16.04 AMI from AWS and 20GB of EBS storage. I also setup firewall rules for VNC port 5900 and SSH port 22 to the instance. I used Remmina VNC desktop client to connect to the machine with color depth set to 16bpp.

### Usage
1. On the remote machine, establish a VNC server and make sure you can interact with a shared browsing window (easier to navigate a demo this way).
2. Also login via an SSH session to the same remote machine.
3. Install `pavucontrol` via `sudo apt install pavucontrol` and open it by running it in the command line.
4. Open a browser session and find a page with an audio source like Youtube. Play a source and monitor that the output tab of the `pavucontrol` shows a changing audio meter. This means that the OS is recognizing audio and is piping it to a fake output driver.
5. In `pavucontrol`, navigate to the last tab and setup a `monitor` on the output audio stream. You should notice now that the monitor microphone input now tracks the audio being played on the output device.
6. Locally, run `./install_deps.sh` and run `go build ./` to generate a binary.
7. SCP up the binary named `portaudiosend` and the `install_deps.sh` script to the remote machine.
8. On the remote machine (SSH session), run `./install_deps.sh` (do not run the binary yet).
9. On the local machine, open a web browser to this jsfiddle address (https://jsfiddle.net/z7ms3u5r/). This is a window that will act as a "client" connection.
10. Copy the entire `Browser base64 Session Description` textbox's contents (make sure to get all contents, otherwise there will be an error).
11. Paste the contents to a text file called `sdp.txt` and SCP that file up to the VNC server machine.
12. In the SSH session, run `cat sdp.txt | ./portaudiosend` to start the audio sending Golang application with the proper session description.
13. Wait for the program to output a corresponding session description in stdout and copy it to your clipboard (the entire thing).
14. Paste that session description in the `Golang base64 Session Description` textbox on your local browser JSfiddle example.
15. Click `Start Session` to begin the session. If the connection works, an audio element should appear on the Jsfiddle page and audio will pipe through to the client.

